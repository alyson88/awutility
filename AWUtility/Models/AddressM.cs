﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;

namespace AWUtility.Models
{
    public class AddressM
    {
        public int Id { get; set; } = 0;

        public string GUId { get; set; } = "";

        //public int UserId { get; set; } = ((UserSessionM)HttpContext.Current.Session["UserSession"]).UserId;

        [Display(Name = "Label")]
        public string Label { get; set; } = "";

        [Display(Name = "Address")]
        public string AddressLine1 { get; set; } = "";

        [Display(Name = "Address")]
        public string AddressLine2 { get; set; } = "";

        [Display(Name = "City")]
        public string City { get; set; } = "";

        [Display(Name = "State")]
        public string State { get; set; } = "";

        [Display(Name = "Postal Code")]
        public string PostalCode { get; set; } = "";

        [Display(Name = "Country")]
        public string Country { get; set; } = "";

        //[Display(Name = "States")]
        //public List<SelectListItem> StatesSelectList { get; set; } = new List<SelectListItem>();


        // End
    }
}
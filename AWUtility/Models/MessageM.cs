﻿using AWUtility.Enums;
using System.ComponentModel.DataAnnotations;

namespace AWUtility.Models
{
    public class MessageM
    {
        [Display(Name = "Type")]
        public MessageType Type { get; set; } = MessageType.Information;

        [Display(Name = "Message")]
        public string Text { get; set; } = "";


        // End
    }
}
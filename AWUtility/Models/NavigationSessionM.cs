﻿using System.ComponentModel.DataAnnotations;

namespace AWUtility.Models
{
    public class NavigationSessionM
    {
        [DataType(DataType.Url)]
        public string CurrentUrl { get; set; } = "";

        [DataType(DataType.Url)]
        public string PreviousUrl { get; set; } = "";

        [DataType(DataType.Url)]
        public string ReturnUrl { get; set; } = "";

        [DataType(DataType.Url)]
        public string NextUrl { get; set; } = "";


        // End
    }
}
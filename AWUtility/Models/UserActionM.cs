﻿using AWUtility.Enums;
using System;
using System.ComponentModel.DataAnnotations;

namespace AWUtility.Models
{
    [Serializable]
    public class UserActionM
    {
        // Calculated properties:

        [Display(Name = "Current Date and Time (UTC)")]
        public DateTime UtcDateTimeNow
        {
            get
            {
                return DateTime.UtcNow;
            }
        }


        [Display(Name = "User ID")]
        public int UserId { get; set; } = 0;

        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Display(Name = "Type")]
        public ActionType Type { get; set; } = ActionType.View;

        [Display(Name = "Success")]
        public bool WasSuccessful { get; set; } = false;

        [Display(Name = "Description")]
        public string Description { get; set; }


        // End
    }
}
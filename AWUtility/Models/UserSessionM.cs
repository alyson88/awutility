﻿using AWUtility.Enums;
using System;
using System.ComponentModel.DataAnnotations;

namespace AWUtility.Models
{
    public class UserSessionM
    {
        public int UserId { get; set; } = 0;

        public string UserGUId { get; set; } = "";

        [Display(Name = "Username")]
        public string Username { get; set; } = "";

        [Display(Name = "First name")]
        public string UserFirstName { get; set; } = "";

        [Display(Name = "Last name")]
        public string UserLastName { get; set; } = "";

        [Display(Name = "Status")]
        public UserStatus UserStatus { get; set; } = UserStatus.Inactive;

        [Display(Name = "Type")]
        public UserType UserType { get; set; } = UserType.ReadOnly;

        [Display(Name = "Name")]
        public string DisplayName { get; set; } = "";

        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email address")]
        public string UserEmail { get; set; } = "";

        [Display(Name = "Time Zone")]
        public TimeZoneInfo TimeZone { get; set; }


        // Calculated properties:

        public string FullName
        {
            get
            {
                var result = "";

                if (!string.IsNullOrEmpty(UserFirstName) || !string.IsNullOrEmpty(UserLastName))
                {
                    if (!string.IsNullOrEmpty(UserFirstName) && !string.IsNullOrEmpty(UserLastName))
                    {
                        result = UserFirstName + " " + UserLastName;
                    }
                    else if (string.IsNullOrEmpty(UserLastName))
                    {
                        result = UserFirstName;
                    }
                    else if (string.IsNullOrEmpty(UserFirstName))
                    {
                        result = UserLastName;
                    }
                }

                return result;
            }
        }


        // End
    }
}
﻿using System.ComponentModel.DataAnnotations;

namespace AWUtility.Models
{
    public class PhoneM
    {
        public int Id { get; set; }


        [Display(Name = "Label")]
        public string Label { get; set; }

        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Number")]
        public string Number { get; set; }


        // End
    }
}
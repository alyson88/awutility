﻿using AWUtility.Enums;
using System;
using System.ComponentModel.DataAnnotations;

namespace AWUtility.Models
{
    public class UserM
    {
        public UserM (string timeZoneId = "")
        {
            if (string.IsNullOrEmpty(timeZoneId))
            {
                TimeZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
            }
            else
            {
                TimeZone = TimeZoneInfo.FindSystemTimeZoneById(timeZoneId);
            }
        }

        public int Id { get; set; } = 0;
        public string GUId { get; set; } = "";


        //[Required(ErrorMessage = "A first name is required.")]
        [Display(Name = "First name")]
        public string FirstName { get; set; } = "";

        [Display(Name = "Middle name")]
        public string MiddleName { get; set; } = "";

        [Display(Name = "Last name")]
        public string LastName { get; set; } = "";

        [Display(Name = "Username")]
        public string Username { get; set; } = "";

        [Display(Name = "Nickname")]
        public string Nickname { get; set; } = "";

        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email address")]
        public string Email { get; set; } = "";

        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; } = "";

        [DataType(DataType.Password)]
        public string CryptoPassword { get; set; } = "";

        //[DataType(DataType.PhoneNumber)]
        //[Display(Name = "Phone")]
        //public string Phone { get; set; } = "";

        //[DataType(DataType.PhoneNumber)]
        //[Display(Name = "Mobile phone")]
        //public string Mobile { get; set; } = "";

        //[Display(Name = "Date of birth")]
        //public DateTime DateOfBirth { get; set; }

        [Display(Name = "Type")]
        public UserType Type { get; set; } = UserType.ReadOnly;

        [Display(Name = "Status")]
        public UserStatus Status { get; set; } = UserStatus.Inactive;

        [Display(Name = "Verification Code")]
        public string VerificationCode { get; set; }

        [DataType(DataType.DateTime)]
        [Display(Name = "Created (UTC)")]
        public DateTime UtcCreatedOn { get; set; }

        [DataType(DataType.DateTime)]
        [Display(Name = "Updated (UTC)")]
        public DateTime UtcEditedOn { get; set; }

        [DataType(DataType.DateTime)]
        [Display(Name = "Latest log in (UTC)")]
        public DateTime UtcLatestLogIn { get; set; }

        [DataType(DataType.DateTime)]
        [Display(Name = "Previous log in (UTC)")]
        public DateTime UtcPreviousLogIn { get; set; }

        [DataType(DataType.Date)]
        public DateTime UtcVerificationCodeExpiresOn { get; set; }

        [Display(Name = "Time zone")]
        public TimeZoneInfo TimeZone { get; set; } = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");


        // Calculated Properties:

        [DataType(DataType.Date)]
        [Display(Name = "Created")]
        public DateTime CreatedOn
        {
            get
            {
                return TimeZoneInfo.ConvertTimeFromUtc(UtcCreatedOn, TimeZone);
            }
        }

        [DataType(DataType.Date)]
        [Display(Name = "Updated")]
        public DateTime EditedOn
        {
            get
            {
                return TimeZoneInfo.ConvertTimeFromUtc(UtcEditedOn, TimeZone);
            }
        }

        [DataType(DataType.DateTime)]
        [Display(Name = "Latest log in")]
        public DateTime LatestLogIn
        {
            get
            {
                return TimeZoneInfo.ConvertTimeFromUtc(UtcLatestLogIn, TimeZone);
            }
        }

        [DataType(DataType.DateTime)]
        [Display(Name = "Previous log in")]
        public DateTime PreviousLogIn
        {
            get
            {
                return TimeZoneInfo.ConvertTimeFromUtc(UtcPreviousLogIn, TimeZone);
            }
        }

        [DataType(DataType.DateTime)]
        [Display(Name = "Verification code expires")]
        public DateTime VerificationCodeExpiresOn
        {
            get
            {
                return TimeZoneInfo.ConvertTimeFromUtc(UtcVerificationCodeExpiresOn, TimeZone);
            }
        }


        // End
    }
}
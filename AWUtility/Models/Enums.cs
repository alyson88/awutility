﻿using System.ComponentModel.DataAnnotations;

namespace AWUtility.Enums
{
    public enum MessageType
    {
        [Display(Name = "Error")]
        Error = 0,
        
        [Display(Name = "Warning")]
        Warning = 1,

        [Display(Name = "Information")]
        Information = 2,

        [Display(Name = "Success")]
        Success = 3
    }

    public enum AccountType
    {
        [Display(Name = "Read Only")]
        ReadOnly = 0,

        [Display(Name = "Basic")]
        Basic = 1,

        [Display(Name = "Deluxe")]
        Deluxe = 2
    }

    public enum UserType
    {
        [Display(Name = "Read Only")]
        ReadOnly = 0,

        [Display(Name = "Basic")]
        Basic = 1,

        [Display(Name = "Deluxe")]
        Deluxe = 2,

        [Display(Name = "Admin")]
        Admin = 3,

        [Display(Name = "Super Admin")]
        SuperAdmin = 4,

        [Display(Name = "Owner")]
        Owner = 5
    }

    public enum UserStatus
    {
        [Display(Name = "Unverified")]
        Unverified = 0,

        [Display(Name = "Inactive")]
        Inactive = 1,

        [Display(Name = "Active")]
        Active = 2,

        [Display(Name = "Locked")]
        Locked = 3,

        [Display(Name = "Archived")]
        Archived = 4
    }

    public enum ActionType
    {

        [Display(Name = "Delete")]
        Delete = 0,

        [Display(Name = "Edit")]
        Edit = 1,

        [Display(Name = "View")]
        View = 2,

        [Display(Name = "Create")]
        Create = 3,

        [Display(Name = "LogIn")]
        LogIn = 4,

        [Display(Name = "LogOut")]
        LogOut = 5
    }

    public enum Month
    {
        [Display(Name = "Not Applicable")]
        NotApplicable = 0,

        [Display(Name = "January")]
        January = 1,
        
        [Display(Name = "February")]
        February = 2,

        [Display(Name = "March")]
        March = 3,

        [Display(Name = "April")]
        April = 4,

        [Display(Name = "May")]
        May = 5,

        [Display(Name = "June")]
        June = 6,

        [Display(Name = "July")]
        July = 7,

        [Display(Name = "August")]
        August = 8,

        [Display(Name = "September")]
        September = 9,

        [Display(Name = "October")]
        October = 10,

        [Display(Name = "November")]
        November = 11,

        [Display(Name = "December")]
        December = 12
    }

    public enum DayOfTheMonth
    {
        [Display(Name = "Not Applicable")]
        NotApplicable = 0,

        [Display(Name = "1st")]
        First = 1,

        [Display(Name = "2nd")]
        Second = 2,

        [Display(Name = "3rd")]
        Third = 3,

        [Display(Name = "4th")]
        Fourth = 4,

        [Display(Name = "5th")]
        Fifth = 5,

        [Display(Name = "6th")]
        Sixth = 6,

        [Display(Name = "7th")]
        Seventh = 7,

        [Display(Name = "8th")]
        Eighth = 8,

        [Display(Name = "9th")]
        Ninth = 9,

        [Display(Name = "10th")]
        Tenth = 10,

        [Display(Name = "11th")]
        Eleventh = 11,

        [Display(Name = "12th")]
        Twelfth = 12,

        [Display(Name = "13th")]
        Thirteenth = 13,

        [Display(Name = "14th")]
        Fourteenth = 14,

        [Display(Name = "15th")]
        Fifteenth = 15,

        [Display(Name = "16th")]
        Sixteenth = 16,

        [Display(Name = "17th")]
        Seventeenth = 17,

        [Display(Name = "18th")]
        Eighteenth = 18,

        [Display(Name = "19th")]
        Ninteenth = 19,

        [Display(Name = "20th")]
        Twentieth = 20,

        [Display(Name = "21st")]
        TwentyFirst = 21,

        [Display(Name = "22nd")]
        TwentySecond = 22,

        [Display(Name = "23rd")]
        TwentyThird = 23,

        [Display(Name = "24th")]
        TwentyFourth = 24,

        [Display(Name = "25th")]
        TwentyFifth = 25,

        [Display(Name = "26th")]
        TwentySixth = 26,

        [Display(Name = "27th")]
        TwentySeventh = 27,

        [Display(Name = "28th")]
        TwentyEighth = 28,

        [Display(Name = "29th")]
        TwentyNinth = 29,

        [Display(Name = "30th")]
        Thirtieth = 30,

        [Display(Name = "31st")]
        ThirtyFirst = 31,

        [Display(Name = "Last day of the month")]
        LastDayOfTheMonth = 32
    }

    public enum Status
    {
        [Display(Name = "Inactive")]
        Inactive = 0,

        [Display(Name = "Active")]
        Active = 1,

        [Display(Name = "On Hold")]
        OnHold = 2,

        [Display(Name = "Archived")]
        Archived = 3
    }


    // End
}
﻿using System.IO;
using System.Web.Mvc;

namespace AWUtility.EmailManager
{
    public class BuildEmailString : Controller
    {
        public static string RenderMVCViewToString(ControllerContext context, string viewPath, object viewModel = null, bool partialView = false)
        {
            ViewEngineResult viewEngineResult;

            if (partialView)
            {
                viewEngineResult = ViewEngines.Engines.FindPartialView(context, viewPath);
            }
            else
            {
                viewEngineResult = ViewEngines.Engines.FindView(context, viewPath, null);
            }

            if (viewEngineResult == null)
            {
                throw new FileNotFoundException("View cannot be found.");
            }

            context.Controller.ViewData.Model = viewModel;

            var sw = new StringWriter();
            using (sw)
            {
                var ctx = new ViewContext(context, viewEngineResult.View, context.Controller.ViewData, context.Controller.TempData, sw);
                viewEngineResult.View.Render(ctx, sw);
            }

            return sw.ToString();
        }
    }
}
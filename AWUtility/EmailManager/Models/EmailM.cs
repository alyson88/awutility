﻿using MimeKit;
using System.Net.Mail;

namespace AWUtility.EmailManager.Models
{
    public class EmailM
    {
        public EmailM()
        {
            EnableSsl = false;
            UseDefaultCredentials = false;
        }

        public string MailServer { get; set; }

        public int Port { get; set; }

        public bool EnableSsl { get; set; }

        public bool UseDefaultCredentials { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }


        public MailMessage MailMessage { get; set; } = new MailMessage();

        public MimeMessage MimeMessage { get; set; } = new MimeMessage();

    }
}
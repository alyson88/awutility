﻿using AWUtility.EmailManager.Models;
using System;
using System.Net;
//using System.Net.Mail;
//using MailKit.Net.Smtp;

namespace AWUtility.EmailManager
{
    public class SendEmail
    {
        public static bool SendMail(EmailM email)
        {
            try
            {
                if (!string.IsNullOrEmpty(email.MailMessage.To.ToString()) && string.IsNullOrEmpty(email.MimeMessage.To.ToString()))
                {
                    var smtpClient = new System.Net.Mail.SmtpClient(email.MailServer)
                    {
                        Credentials = new NetworkCredential(email.Username, email.Password),
                        Port = email.Port,
                        EnableSsl = email.EnableSsl,
                        UseDefaultCredentials = email.UseDefaultCredentials,
                        DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network
                    };

                    smtpClient.Send(email.MailMessage);

                    return true;
                }
                else if (!string.IsNullOrEmpty(email.MimeMessage.To.ToString()) && string.IsNullOrEmpty(email.MailMessage.To.ToString()))
                {
                    using (var smtpClient = new MailKit.Net.Smtp.SmtpClient())
                    {
                        smtpClient.Connect(email.MailServer, email.Port, email.EnableSsl);
                        smtpClient.Authenticate(email.Username, email.Password);
                        smtpClient.Send(email.MimeMessage);
                        smtpClient.Disconnect(true);
                    }
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                throw new Exception(String.Format($"{System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.FullName}.{System.Reflection.MethodBase.GetCurrentMethod().Name}: {ex.Message} {ex.StackTrace}"));
            }
        }

    }
}
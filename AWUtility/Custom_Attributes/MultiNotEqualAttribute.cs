﻿using System.ComponentModel.DataAnnotations;

namespace AWUtility.Custom_Attributes
{
    public class MultiNotEqualAttribute : ValidationAttribute
    {
        private readonly string _otherProperty1;
        private readonly string _otherProperty2;
        public MultiNotEqualAttribute(string otherProperty1, string otherProperty2)
        {
            _otherProperty1 = otherProperty1;
            _otherProperty2 = otherProperty2;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var property1 = validationContext.ObjectType.GetProperty(_otherProperty1);
            var property2 = validationContext.ObjectType.GetProperty(_otherProperty2);
            if (property1 == null) {
                return new ValidationResult(string.Format("Unknown property: {0}", _otherProperty1));
            }
            if (property2 == null) {
                return new ValidationResult(string.Format("Unknown property: {0}", _otherProperty2));
            }
            var otherValue1 = property1.GetValue(validationContext.ObjectInstance, null);
            var otherValue2 = property2.GetValue(validationContext.ObjectInstance, null);

            if (object.Equals(value, otherValue1) || object.Equals(value, otherValue2)) {
                return new ValidationResult($"This value cannot match the value of {_otherProperty1} or {_otherProperty2}");
            }
            return ValidationResult.Success;
        }
    }
}
﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AWUtility.Custom_Attributes
{
    public class CustomNumberRangeAttribute : ValidationAttribute
    {
        private readonly int _minNumber;
        private readonly string _baseNumberForMax; //so you can set the max to be less or more than this base number
        private readonly int _plusOrMinusBase; //how much more or less than the base number you want the max to be
        public CustomNumberRangeAttribute(int minNumber , string baseNumberForMax , int plusOrMinusBase)
        {
            _minNumber = minNumber;
            _baseNumberForMax = baseNumberForMax;
            _plusOrMinusBase = plusOrMinusBase;
        }

        protected override ValidationResult IsValid(object value , ValidationContext validationContext)
        {
            var number2 = validationContext.ObjectType.GetProperty(_baseNumberForMax);
            if (number2 == null)
            {
                return new ValidationResult(string.Format("Unknown property: {0}" , _baseNumberForMax));
            }
            var numberValue2 = number2.GetValue(validationContext.ObjectInstance , null);

            if (Convert.ToInt32(value) < _minNumber || Convert.ToInt32(value) > (Convert.ToInt32(numberValue2) + _plusOrMinusBase))
            {
                return new ValidationResult($"Number must be between {_minNumber} and {Convert.ToInt32(numberValue2) + _plusOrMinusBase}");
            }
            return ValidationResult.Success;
        }
    }
}
﻿using System.Web.Mvc;
using System.Web.Mvc.Filters;
using System.Web.Routing;

namespace AWUtility.Custom_Attributes
{
    public class LoggedInAttribute : ActionFilterAttribute, IAuthenticationFilter
    {
        public void OnAuthentication(AuthenticationContext filterContext) {
            if (filterContext.HttpContext.Session["UserSession"] == null)
            {
                filterContext.Result = new HttpUnauthorizedResult();
            }
        }

        public void OnAuthenticationChallenge(AuthenticationChallengeContext filterContext) {
            if (filterContext.Result == null)
            {
                filterContext.Result = new RedirectToRouteResult(
                    new RouteValueDictionary
                    {
                    { "controller", "User" },
                    { "action", "LogIn" }
                    }
                );
            }
            else if (filterContext.Result is HttpUnauthorizedResult)
            {
                filterContext.Controller.TempData["Error"] = "You must log in to view that page.";

                filterContext.Result = new RedirectToRouteResult(
                    new RouteValueDictionary
                    {
                    { "controller", "User" },
                    { "action", "LogIn" }
                    }
                );
            }
        }


        // End
    }
}
﻿using System.Web.Mvc.Filters;
using System.Web.Mvc;
using AWUtility.Enums;
using AWUtility.Models;
using System.Web.Routing;

namespace AWUtility.Custom_Attributes
{
    public class OwnerUserAccessAttribute : ActionFilterAttribute, IAuthenticationFilter
    {
        public void OnAuthentication(AuthenticationContext filterContext)
        {
            if ((((UserSessionM)filterContext.HttpContext.Session["UserSession"]).UserType == UserType.ReadOnly) || (((UserSessionM)filterContext.HttpContext.Session["UserSession"]).UserType == UserType.Basic) || (((UserSessionM)filterContext.HttpContext.Session["UserSession"]).UserType == UserType.Deluxe) || (((UserSessionM)filterContext.HttpContext.Session["UserSession"]).UserType == UserType.Admin) || (((UserSessionM)filterContext.HttpContext.Session["UserSession"]).UserType == UserType.SuperAdmin))
            {
                filterContext.Result = new HttpUnauthorizedResult();
            }
        }

        public void OnAuthenticationChallenge(AuthenticationChallengeContext filterContext)
        {
            if (filterContext.Result == null)
            {
                filterContext.Result = new RedirectToRouteResult(
                    new RouteValueDictionary
                    {
                    { "controller", "User" },
                    { "action", "LogIn" }
                    }
                );
            }
            else if (filterContext.Result is HttpUnauthorizedResult)
            {
                filterContext.Controller.TempData["Error"] = "You must be an Owner to view that page.";

                filterContext.Result = new RedirectToRouteResult(
                    new RouteValueDictionary
                    {
                    { "controller", "User" },
                    { "action", "LogIn" }
                    }
                );
            }
        }


        // End
    }
}
﻿using System.ComponentModel.DataAnnotations;

namespace AWUtility.Custom_Attributes
{
    public class RequiredIfAttribute : RequiredAttribute
    {
        private readonly string _otherProperty;
        private readonly object _desiredValue;

        public RequiredIfAttribute(string propertyName , object desiredValue)
        {
            _otherProperty = propertyName;
            _desiredValue = desiredValue;
        }

        protected override ValidationResult IsValid(object value , ValidationContext validationContext)
        {
            var property = validationContext.ObjectType.GetProperty(_otherProperty);
            if (property == null)
            {
                return new ValidationResult(string.Format("Unknown property: {0}" , _otherProperty));
            }

            var propertyValue = property.GetValue(validationContext.ObjectInstance , null);

            if (propertyValue.ToString() == _desiredValue.ToString())
            {
                ValidationResult result = base.IsValid(value , validationContext);
                return result;
            }

            return ValidationResult.Success;
        }
    }
}
﻿using System.ComponentModel.DataAnnotations;

namespace AWUtility.Custom_Attributes
{
    public class NotEqualAttribute : ValidationAttribute
    {
        private readonly string _otherProperty;
        public NotEqualAttribute(string otherProperty)
        {
            _otherProperty = otherProperty;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var property = validationContext.ObjectType.GetProperty(_otherProperty);
            if (property == null) {
                return new ValidationResult(string.Format("Unknown property: {0}", _otherProperty));
            }
            var otherValue = property.GetValue(validationContext.ObjectInstance, null);

            if (object.Equals(value, otherValue)) {
                return new ValidationResult($"This value cannot match the {_otherProperty} value.");
            }
            return ValidationResult.Success;
        }
    }
}
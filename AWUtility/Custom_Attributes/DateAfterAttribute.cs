﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AWUtility.Custom_Attributes
{
    public class DateAfterAttribute : ValidationAttribute
    {
        private readonly string _dateAfter;

        public DateAfterAttribute(string dateAfter)
        {
            _dateAfter = dateAfter;
        }

        protected override ValidationResult IsValid(object value , ValidationContext validationContext)
        {
            if (value != null)
            {
                var property = validationContext.ObjectType.GetProperty(_dateAfter);
                if (property == null)
                {
                    return new ValidationResult(string.Format($"Unknown property: {_dateAfter}"));
                }
                var dateAfterValue = property.GetValue(validationContext.ObjectInstance , null);
                DateTime dateAfterDate = (DateTime)dateAfterValue;

                if ((DateTime)value <= dateAfterDate)
                {
                    return new ValidationResult($"This date must be after {dateAfterDate.ToString("M/d/yy")}");
                }
            }
            return ValidationResult.Success;
        }
    }
}
﻿using AWUtility.DatabaseManager.Models;
using System;
using System.Collections.Generic;

namespace AWUtility.DatabaseManager
{
    public class DbParameterList
    {
        public List<DbParameter> ParameterList { get; private set; }

        public DbParameterList()
        {
            ParameterList = new List<DbParameter>();
        }

        public void AddToList(string sqlVariable, object value, object dbType = null)
        {
            try {
                var parameter = new DbParameter {
                    Name = sqlVariable,
                    Value = value,
                    DbType = dbType
                };
                ParameterList.Add(parameter);
            } catch (Exception ex) {
                throw new Exception("DbParameterList.AddToList:  " + ex.Message);
            }
        }
    }
}
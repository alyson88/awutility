﻿using System;

namespace AWUtility.DatabaseManager.Models
{
    [Serializable]
    public class DbParameter
    {
        public string Name { get; set; }
        public object Value { get; set; }
        public object DbType { get; set; }
        public int Length { get; set; }
    }
}
﻿using AWUtility.DatabaseManager.Models;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;

namespace AWUtility.DatabaseManager
{
    public class DbAccessManager
    {
        public string DbName { get; set; }
        public string PreProcessorCommand { get; set; }
        public string PostProcessorCommand { get; set; }
        public int CommandTimeout { get; set; }
        public int ConnectionTimeout { get; set; }

        public DbAccessManager()
        {
            PreProcessorCommand = "";
            PostProcessorCommand = "";
        }

        public string GetPreprocessorCommand()
        {
            try
            {
                var command = new System.Text.StringBuilder();

                if (!string.IsNullOrEmpty(DbName))
                {
                    command.Append($"USE {DbName}; ");
                }
                if (!string.IsNullOrEmpty(PreProcessorCommand))
                {
                    command.Append($"{PreProcessorCommand};");
                }
                return command.ToString();
            }
            catch (Exception ex)
            {
                throw new Exception($"{MethodBase.GetCurrentMethod().DeclaringType.FullName}.{MethodBase.GetCurrentMethod().Name}:{ex.Message} {ex.StackTrace}");
            }
        }

        public DataView ExecuteQueryToGet(string connectionString, string sqlQueryString, CommandType cmdType, DbParameterList parameters = null)
        {
            try
            {
                var dt = new DataTable();
                using (var conn = new SqlConnection(connectionString))
                {
                    var cmd = new SqlCommand($"{GetPreprocessorCommand()} {sqlQueryString} {PostProcessorCommand}", conn);
                    cmd.CommandType = cmdType;

                    if (CommandTimeout > 0)
                        cmd.CommandTimeout = CommandTimeout;

                    if (parameters != null)
                    {
                        foreach (DbParameter parameter in parameters.ParameterList)
                        {
                            var sqlParameter = new SqlParameter();
                            sqlParameter.ParameterName = parameter.Name;
                            sqlParameter.Value = parameter.Value;
                            if (parameter.DbType != null)
                            {
                                sqlParameter.DbType = (DbType)parameter.DbType;
                            }
                            cmd.Parameters.Add(sqlParameter);
                        }
                    }
                    using (var adapter = new SqlDataAdapter(cmd))
                    {
                        adapter.Fill(dt);
                    }

                    var dv = new DataView();
                    dv = dt.DefaultView;

                    return dv;
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"{MethodBase.GetCurrentMethod().DeclaringType.FullName}.{MethodBase.GetCurrentMethod().Name}:{ex.Message} {ex.StackTrace} for SQL {sqlQueryString} using connection {connectionString}");
            }
        }

        public int ExecuteQueryToGetCount(string connectionString, string sqlQueryString, CommandType cmdType, DbParameterList parameters = null)
        {
            try
            {
                var dt = new DataTable();
                using (var conn = new SqlConnection(connectionString))
                {
                    var cmd = new SqlCommand($"{GetPreprocessorCommand()} {sqlQueryString} {PostProcessorCommand}", conn);
                    cmd.CommandType = cmdType;

                    if (CommandTimeout > 0)
                        cmd.CommandTimeout = CommandTimeout;

                    if (parameters != null)
                    {
                        foreach (DbParameter parameter in parameters.ParameterList)
                        {
                            var sqlParameter = new SqlParameter();
                            sqlParameter.ParameterName = parameter.Name;
                            sqlParameter.Value = parameter.Value;
                            if (parameter.DbType != null)
                            {
                                sqlParameter.DbType = (DbType)parameter.DbType;
                            }
                            cmd.Parameters.Add(sqlParameter);
                        }
                    }
                    using (var adapter = new SqlDataAdapter(cmd))
                    {
                        adapter.Fill(dt);
                    }
                    return (int)cmd.ExecuteScalar();
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"{MethodBase.GetCurrentMethod().DeclaringType.FullName}.{MethodBase.GetCurrentMethod().Name}:{ex.Message} {ex.StackTrace} for SQL {sqlQueryString} using connection {connectionString}");
            }
        }

        public int ExecuteQueryToInsert(string connectionString, string sqlQueryString, CommandType cmdType, out object newestID, DbParameterList parameters = null)
        {
            try
            {
                using (var conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    var transaction = conn.BeginTransaction();
                    var cmd = new SqlCommand($"{GetPreprocessorCommand()} {sqlQueryString} {PostProcessorCommand}", conn, transaction);
                    cmd.CommandType = cmdType;

                    if (CommandTimeout > 0)
                    {
                        cmd.CommandTimeout = CommandTimeout;
                    }
                    if (parameters != null)
                    {
                        foreach (DbParameter parameter in parameters.ParameterList)
                        {
                            var sqlParameter = new SqlParameter();
                            sqlParameter.ParameterName = parameter.Name;
                            sqlParameter.Value = parameter.Value;
                            if (parameter.DbType != null)
                            {
                                sqlParameter.DbType = (DbType)parameter.DbType;
                            }
                            cmd.Parameters.Add(sqlParameter);
                        }
                    }
                    int records = cmd.ExecuteNonQuery();
                    transaction.Commit();
                    newestID = null;
                    if (sqlQueryString.StartsWith("INSERT"))
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = "SELECT @@IDENTITY";
                        newestID = cmd.ExecuteScalar();
                    }
                    return records;
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"{MethodBase.GetCurrentMethod().DeclaringType.FullName}.{MethodBase.GetCurrentMethod().Name}:{ex.Message} {ex.StackTrace}");
            }
        }

        public int ExecuteQueryToUpdate(string connectionString, string sqlQueryString, CommandType cmdType, DbParameterList parameters = null)
        {
            try
            {
                using (var conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    var transaction = conn.BeginTransaction();
                    var cmd = new SqlCommand($"{GetPreprocessorCommand()} {sqlQueryString} {PostProcessorCommand}", conn, transaction);
                    cmd.CommandType = cmdType;

                    if (CommandTimeout > 0)
                    {
                        cmd.CommandTimeout = CommandTimeout;
                    }
                    if (parameters != null)
                    {
                        foreach (DbParameter parameter in parameters.ParameterList)
                        {
                            var sqlParameter = new SqlParameter();
                            sqlParameter.ParameterName = parameter.Name;
                            sqlParameter.Value = parameter.Value;
                            if (parameter.DbType != null)
                            {
                                sqlParameter.DbType = (DbType)parameter.DbType;
                            }
                            cmd.Parameters.Add(sqlParameter);
                        }
                    }
                    //int records = cmd.ExecuteNonQuery();
                    return cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"{MethodBase.GetCurrentMethod().DeclaringType.FullName}.{MethodBase.GetCurrentMethod().Name}:{ex.Message} {ex.StackTrace}");
            }
        }

        public int ExecuteQueryToDelete(string connectionString, string sqlQueryString, CommandType cmdType, DbParameterList parameters = null)
        {
            try
            {
                using (var conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    var transaction = conn.BeginTransaction();
                    var cmd = new SqlCommand($"{GetPreprocessorCommand()} {sqlQueryString} {PostProcessorCommand}", conn, transaction);
                    cmd.CommandType = cmdType;

                    if (CommandTimeout > 0)
                    {
                        cmd.CommandTimeout = CommandTimeout;
                    }
                    if (parameters != null)
                    {
                        foreach (DbParameter parameter in parameters.ParameterList)
                        {
                            var sqlParameter = new SqlParameter();
                            sqlParameter.ParameterName = parameter.Name;
                            sqlParameter.Value = parameter.Value;
                            if (parameter.DbType != null)
                            {
                                sqlParameter.DbType = (DbType)parameter.DbType;
                            }
                            cmd.Parameters.Add(sqlParameter);
                        }
                    }
                    return cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"{MethodBase.GetCurrentMethod().DeclaringType.FullName}.{MethodBase.GetCurrentMethod().Name}:{ex.Message} {ex.StackTrace}");
            }
        }
    }
}
﻿using System;
using System.Reflection;
using System.Text.RegularExpressions;

namespace AWUtility.Custom_Helpers
{
    public class StringHelper
    {
        public string RemoveSpecialCharactersFromString(string stringToEdit)
        {
            try
            {
                return Regex.Replace(stringToEdit , @"[^0-9a-zA-Z]+" , "");
            }
            catch (Exception ex)
            {
                throw new Exception($"{MethodBase.GetCurrentMethod().DeclaringType.FullName}.{MethodBase.GetCurrentMethod().Name}: {ex.Message} {ex.StackTrace}");
            }
        }

        public string ReplaceSpecialCharactersWithSpacesInString(string stringToEdit)
        {
            try
            {
                return Regex.Replace(stringToEdit , @"[^0-9a-zA-Z]+" , " ");
            }
            catch (Exception ex)
            {
                throw new Exception($"{MethodBase.GetCurrentMethod().DeclaringType.FullName}.{MethodBase.GetCurrentMethod().Name}: {ex.Message} {ex.StackTrace}");
            }
        }

        public string ReplaceSpecialCharactersWithUnderscoresInString(string stringToEdit)
        {
            try
            {
                return Regex.Replace(stringToEdit , @"[^0-9a-zA-Z]+" , "_");
            }
            catch (Exception ex)
            {
                throw new Exception($"{MethodBase.GetCurrentMethod().DeclaringType.FullName}.{MethodBase.GetCurrentMethod().Name}: {ex.Message} {ex.StackTrace}");
            }
        }
    }
}
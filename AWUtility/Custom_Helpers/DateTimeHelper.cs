﻿using System;
using System.Reflection;

namespace AWUtility.Custom_Helpers
{
    public class DateTimeHelper
    {
        public int GetAge(DateTime dateOfBirth, string timeZoneId, DateTime? asOfThisDate = null) {
            try
            {
                if (asOfThisDate == null)
                {
                    var utcToday = DateTime.UtcNow;
                    var timeZone = TimeZoneInfo.FindSystemTimeZoneById(timeZoneId);
                    var asOfTodaysDate = TimeZoneInfo.ConvertTimeFromUtc(utcToday, timeZone);
                    return (asOfTodaysDate.Date - dateOfBirth.Date).Days / 365;
                }

                return (((DateTime)asOfThisDate).Date - dateOfBirth.Date).Days / 365;
            }
            catch (Exception ex)
            {
                throw new Exception(String.Format($"{MethodBase.GetCurrentMethod().DeclaringType.FullName}.{MethodBase.GetCurrentMethod().Name}: {ex.Message} {ex.StackTrace}"));
            }
        }


    }
}
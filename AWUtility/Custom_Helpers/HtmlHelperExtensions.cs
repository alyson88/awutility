﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace AWUtility.Custom_Helpers
{
    public static class HtmlHelperExtensions
    {
        public static HtmlString DisplayForPhone(this HtmlHelper helper , string phone)
        {
            if (phone == null)
            {
                return new HtmlString(string.Empty);
            }
            string formatted = phone;
            if (phone.Length == 10)
            {
                formatted = $"({phone.Substring(0 , 3)}) {phone.Substring(3 , 3)}-{phone.Substring(6 , 4)}";
            }
            else if (phone.Length == 7)
            {
                formatted = $"{phone.Substring(0 , 3)}-{phone.Substring(3 , 4)}";
            }
            else if (phone.Length == 11)
            {
                formatted = $"{phone.Substring(0 , 1)} ({phone.Substring(1 , 3)}) {phone.Substring(4 , 3)}-{phone.Substring(7 , 4)}";
            }
            string s = $"<a href='tel:{phone}'>{formatted}</a>";
            return new HtmlString(s);
        }

        public static HtmlString DisplayForDate(this HtmlHelper helper , DateTime date)
        {
            var dateString = date.ToString("M/d/yyyy");
            return new HtmlString(dateString);
        }

        public static HtmlString DisplayForDateTime(this HtmlHelper helper , DateTime dateTime)
        {
            var dateTimeString = dateTime.ToString("M/d/yyyy") + " " + dateTime.ToString("h:mm") + " " + dateTime.ToString("tt").ToLower();
            return new HtmlString(dateTimeString);
        }

        public static HtmlString DisplayForMinutes(this HtmlHelper helper , int minutes)
        {
            var minutesString = minutes.ToString() + " min.";
            return new HtmlString(minutesString);
        }

        public static HtmlString DisplayForPercent(this HtmlHelper helper , int number)
        {
            var numberPercentString = number.ToString() + " %";
            return new HtmlString(numberPercentString);
        }

        public static HtmlString DisplayForPercent(this HtmlHelper helper , double number)
        {
            var numberPercentString = number.ToString("0.0") + "%";
            return new HtmlString(numberPercentString);
        }

        public static HtmlString DisplayForEnum(this HtmlHelper helper , Enum enumValue)
        {
            var enumDisplayNameString = enumValue.GetType().GetMember(enumValue.ToString()).First().GetCustomAttribute<DisplayAttribute>().GetName().ToString();
            return new HtmlString(enumDisplayNameString);
        }
    }
}
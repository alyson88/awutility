﻿using System;
using System.Net;

namespace AWUtility.Custom_Helpers
{
    public class InternetAccessCheck
    {
        public bool CanAccessInternet()
        {
            try
            {
                using (var client = new WebClient())
                using (var stream = client.OpenRead("http://www.google.com"))
                {
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
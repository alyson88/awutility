﻿using System;
using System.Text.RegularExpressions;

namespace AWUtility.Security
{
    public class RandomHelper
    {
        public string GenerateRandomString()
        {
            var str = Guid.NewGuid().ToString();
            return Regex.Replace(str, @"[^0-9a-zA-Z]+", "");
        }

        public string GenerateRandomString(int characters)
        {
            var str = GenerateRandomString();
            int strLength = str.Length;
            if (characters > strLength)
            {
                return null;
            }
            int rnd = GenerateRandomInteger(1, strLength - characters + 1);
            return str.Substring(rnd, characters);
        }

        public int GenerateRandomInteger()
        {
            var rnd = new Random();
            return rnd.Next();
        }


        public int GenerateRandomInteger(int minValue, int maxValue)
        {
            var rnd = new Random();
            return rnd.Next(minValue, maxValue);
        }

    }
}
﻿using System;

namespace AWUtility.Security
{
    public class PasswordGenerator
    {
        protected System.Random rGen;
        protected string[] stringCharacters = {"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z",
                                               "1","2","3","4","5","6","7","8","9","0",
                                               "a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"};

        public PasswordGenerator()
        {
            rGen = new System.Random();
        }

        public string GeneratePasswordUpperAndLowerCase(int numOfChar)
        {
            string password = "";
            for (int i = 0; i <= numOfChar; i++) {
                int p = rGen.Next(0, 61);
                password += stringCharacters[p];
            }
            return password;
        }

        public string GeneratePasswordLowerCase(int numOfChar)
        {
            string password = "";
            for (int i = 0; i <= numOfChar; i++) {
                int p = rGen.Next(0, 35);
                password += stringCharacters[p];
            }
            return password.ToLower();
        }
    }
}
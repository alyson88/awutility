﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace AWUtility.Security
{
    public class Crypto
    {
        public static string EncryptPassword(string password) {
            try
            {
                String encryptedPassword = "";
                byte[] textBytes = Encoding.Default.GetBytes(password);
                var cryptHandler = new MD5CryptoServiceProvider();
                byte[] hash = cryptHandler.ComputeHash(textBytes);
                foreach (byte a in hash)
                {
                    if (a < 16)
                        encryptedPassword += "0" + a.ToString("x");
                    else
                        encryptedPassword += a.ToString("x");
                }
                return encryptedPassword;
            }
            catch (Exception ex)
            {
                throw new Exception("Utilities.Security.Crypto.EncryptPassword:  " + ex.Message);
            }
        }

        public static string Encrypt(string key, string stringToEncrypt, bool useHashing) {
            try
            {
                byte[] keyArray;
                byte[] stringToEncryptArray = Encoding.UTF8.GetBytes(stringToEncrypt);

                if (useHashing)
                {
                    var hashmd5 = new MD5CryptoServiceProvider();
                    keyArray = hashmd5.ComputeHash(Encoding.UTF8.GetBytes(key));
                    hashmd5.Clear();
                }
                else
                {
                    keyArray = Encoding.UTF8.GetBytes(key);
                }

                var tdes = new TripleDESCryptoServiceProvider();
                tdes.Key = keyArray;
                tdes.Mode = CipherMode.ECB;
                tdes.Padding = PaddingMode.PKCS7;

                ICryptoTransform cTransform = tdes.CreateEncryptor();
                byte[] resultArray = cTransform.TransformFinalBlock(stringToEncryptArray, 0, stringToEncryptArray.Length);
                tdes.Clear();

                return Convert.ToBase64String(resultArray, 0, resultArray.Length);
            }
            catch (Exception ex)
            {
                throw new Exception("Utilities.Security.Crypto.Encrypt:  " + ex.Message);
            }
        }

        public static string Decrypt(string key, string cipherString, bool useHashing) {
            try
            {
                byte[] keyArray;
                byte[] toEncryptArray = Convert.FromBase64String(cipherString);

                if (useHashing)
                {
                    var hashmd5 = new MD5CryptoServiceProvider();
                    keyArray = hashmd5.ComputeHash(Encoding.UTF8.GetBytes(key));
                    hashmd5.Clear();
                }
                else
                {
                    keyArray = Encoding.UTF8.GetBytes(key);
                }

                var tdes = new TripleDESCryptoServiceProvider();
                tdes.Key = keyArray;
                tdes.Mode = CipherMode.ECB;
                tdes.Padding = PaddingMode.PKCS7;

                ICryptoTransform cTransform = tdes.CreateDecryptor();
                byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
                tdes.Clear();

                return Encoding.UTF8.GetString(resultArray);
            }
            catch (Exception ex)
            {
                throw new Exception("Utilities.Security.Crypto.Decrypt:  " + ex.Message);
            }
        }

        public string EncodeBase64(string data) {
            try
            {
                byte[] encData_byte = new byte[data.Length];
                encData_byte = Encoding.UTF8.GetBytes(data);
                return Convert.ToBase64String(encData_byte);
            }
            catch (Exception ex)
            {
                throw new Exception("Utilities.Security.Crypto.EncodeBase64:  " + ex.Message);
            }
        }

        public static string DecodeBase64(string encodedString) {
            try
            {
                var encoder = new UTF8Encoding();
                var utf8Decode = encoder.GetDecoder();

                byte[] todecode_byte = Convert.FromBase64String(encodedString);
                var charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
                char[] decoded_char = new char[charCount];
                utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);

                return new string(decoded_char);
            }
            catch (Exception ex)
            {
                throw new Exception("Utilities.Security.Crypto.DecodeBase64:  " + ex.Message);
            }
        }

        /// Valid length size is 8 or 12 to generate a 128 or 192 bit key
        /// 8 will generate a 16 char string.  16 x 8(bit) = 128
        /// 12 will generate a 24 char string.  24 x 8(bit) = 192
        /// 128/192 are the only two valid key sizes for .net's tripedes
        public static string GenerateTripleDESKey(int length) {
            try
            {
                byte[] keygen = new byte[length];

                var rng = new RNGCryptoServiceProvider();
                rng.GetBytes(keygen);

                var hexString = new StringBuilder(64);
                for (int counter = 0; counter < keygen.Length; counter++)
                {
                    hexString.Append(string.Format("{0:X2}", keygen[counter]));
                }

                return hexString.ToString();
            }
            catch (Exception ex)
            {
                throw new Exception("Utilities.Security.Crypto.GenerateTripleDESKey:  " + ex.Message);
            }
        }
    }
}